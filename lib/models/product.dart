import 'package:flutter/material.dart';

class Product {
  final String id;
  final String imagePath;
  final Widget page;

  Product({
    @required this.page,
    @required this.id,
    @required this.imagePath,
  });
}
