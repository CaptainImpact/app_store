import 'package:app_store/models/product.dart';
import 'package:app_store/ui/widgets/bottom_navigation/bottom_navigation_bar.dart';
import 'package:app_store/ui/widgets/products_grid.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: ProductsGrid(),
    );
  }
}
