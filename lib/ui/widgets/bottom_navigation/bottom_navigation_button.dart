import 'package:flutter/material.dart';

class BottomNavigationButton extends StatefulWidget {
  final Function function;
  final IconData icon;
  final String title;
  bool isSelected;

  BottomNavigationButton({@required this.function, @required this.icon, @required this.title, this.isSelected = false});

  @override
  _BottomNavigationButtonState createState() => _BottomNavigationButtonState();
}

class _BottomNavigationButtonState extends State<BottomNavigationButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.function();
        setState(() {
          widget.isSelected = !widget.isSelected;
        });
      },
      child: Column(
        children: <Widget>[
          widget.isSelected
              ? Icon(
                  widget.icon,
                  color: Colors.blue,
                )
              : Icon(
                  widget.icon,
                  color: Colors.grey,
                ),
          Text(
            widget.title,
            style: TextStyle(
              color: widget.isSelected ? Colors.blue : Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
