import 'package:app_store/res/icons/my_flutter_app_icons.dart';
import 'package:app_store/ui/widgets/bottom_navigation/bottom_navigation_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBottomNavigationBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: preferredSize.width,
      height: preferredSize.height,
      child: Stack(
        children: [
          Container(
            color: Colors.white.withOpacity(0.9),
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  BottomNavigationButton(
                    title: 'Search',
                    icon: Icons.search,
                    function: () {},
                  ),
                  BottomNavigationButton(
                    title: 'Arcade',
                    icon: MyFlutterApp.apple_arcade,
                    function: () {},
                  ),
                  BottomNavigationButton(
                    title: 'Apps',
                    icon: MyFlutterApp.layers,
                    function: () {},
                  ),
                  BottomNavigationButton(
                    title: 'Games',
                    icon: MyFlutterApp.rocket_icon,
                    function: () {},
                  ),
                  BottomNavigationButton(
                    title: 'Today',
                    icon: MyFlutterApp.phone_icon,
                    function: () {},
                    isSelected: true,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size(double.infinity, 60.0);
}
