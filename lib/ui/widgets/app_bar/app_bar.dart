import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget with PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.close, color: Colors.black,),
      onPressed: () => Navigator.of(context).pop(),
      backgroundColor: Colors.white.withOpacity(0.9),
      mini: true,

    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.zero;
}
