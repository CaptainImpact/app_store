import 'package:app_store/ui/widgets/app_bar/app_bar.dart';
import 'package:app_store/ui/widgets/bottom_navigation/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';

class PageLayout extends StatelessWidget {
  final bool hasAppBar;
  final Widget body;
  final bool hasBottomNavigationBar;

  const PageLayout({
    this.hasAppBar = false,
    this.body,
    this.hasBottomNavigationBar = false,
  });

  @override
  Widget build(BuildContext context) {
    SafeArea closeButton = SafeArea(
      top: true,
      child: RawMaterialButton(
        onPressed: () => Navigator.of(context).pop(),
        fillColor: Color.fromRGBO(105, 105, 105, 1),
        child: Icon(
          Icons.close,
          size: 30.0,
          color: Colors.white,
        ),
        shape: CircleBorder(),
      ),
    );

    return Scaffold(
      body: Stack(
        children: [
          //body
          SafeArea(
            top: true,
            bottom: true,
            child: Container(
              child: body,
              color: Color.fromRGBO(240, 240, 240, 1),
            ),
          ),

          //bottom navigation
          hasBottomNavigationBar
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: MyBottomNavigationBar(),
                )
              : SizedBox(),

          //close button
          Align(
            alignment: Alignment.topLeft,
            child: hasAppBar ? closeButton : SizedBox(),
          ),
        ],
      ),
    );
  }
}
