import 'package:app_store/models/product.dart';
import 'package:app_store/ui/pages/real_plane_page.dart';
import 'package:flutter/material.dart';


class ProductsProvider with ChangeNotifier {
  List<Product> item = [
    Product(
      page: PlanePage(),
      id: PlanePage.tag,
      imagePath:
          'https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    ),
    Product(
      page: Container(color: Colors.red, height: 400,),
      id: 'image2',
      imagePath: 'https://www.hackingwithswift.com/uploads/matrix.jpg',
    ),
    Product(
      page: Container(color: Colors.green, height: 400,),
      id: 'image3',
      imagePath: 'https://www.sciencealert.com/images/2020-02/processed/010-crystals-random_1024.jpg',
    ),
  ];



  Product findById(String id) {
    return item.firstWhere((product) => product.id == id);
  }
}
